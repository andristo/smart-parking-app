package parking.meter.app;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amazonaws.services.iot.client.AWSIotException;
import com.amazonaws.services.iot.client.AWSIotMqttClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


public class ParkingMeterApp {

    private static final Logger logger = Logger.getLogger(ParkingMeterApp.class);

	public static void main(String[] args){
		
		ObjectMapper mapper = new ObjectMapper();
		File file_in = new File("/Users/andrei/DevWork/Projects/parking-meter-app/src/main/resources/sample_data.json");
		
		List<MeterMessage> messages = new ArrayList<MeterMessage>();
		
		try {
            //deserialize sample data JSON file into Java object
			messages = mapper.readValue(file_in, new TypeReference<List<MeterMessage>>(){});
            System.out.println("main - messages = " + messages);
        } 
		catch (IOException e) {
            e.printStackTrace();
        }
		
		//add serialNumber to messages in the list
		int duplicates = 0;
		Integer serialNumber = 1000000;
		Map<String,String> locIdMap = new HashMap<String,String>();
		for(MeterMessage message: messages){
			String locIdKey = message.getMeter().getAddress() + "_" + message.getMeter().getNumber();
			if(locIdMap.containsKey(locIdKey)){
				message.setMeterId(locIdMap.get(locIdKey));
				duplicates++;
			}
			else {
				serialNumber++;
				message.setMeterId(serialNumber.toString());
				locIdMap.put(locIdKey, message.getMeterId());
			}
		}
		System.out.println("main - unique: " + (serialNumber + 1) + " duplicates: " + duplicates);
		
		// create IoT client
		CommandArguments arguments = CommandArguments.parse(args);
		AWSIotMqttClient awsIotClient = ClientUtil.initClient(arguments);
		try {
			awsIotClient.connect();
			//logger.debug("main - connected to IoT");
	        
			for(MeterMessage message: messages){
				String payload = mapper.writeValueAsString(message);
				logger.debug("main - publishing: " + message.getMeterId());
				awsIotClient.publish("ParkingMeterStatus", payload);
			}
		}
		catch (AWSIotException | JsonProcessingException e) {
			logger.error("main - error subscribing to topic, " + e);
		}
		finally {
			try {
				awsIotClient.disconnect();
				logger.info("main - iot connection closed");
			}
			catch (AWSIotException e) {
				logger.error("main - iot error while disconnecting, " + e);				
			}
		}
	}
}
