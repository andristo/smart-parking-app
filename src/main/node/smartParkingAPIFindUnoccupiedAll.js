var AWS = require('aws-sdk');

exports.handler = function(event,context,callback) {

    var params = {
      TableName: 'MeterStatus',
      IndexName: 'IsOccupied-index',
      KeyConditionExpression: 'IsOccupied = :isOccupied',
      ExpressionAttributeValues: {
        ':isOccupied': 'false',
      }
    };

    var docClient = new AWS.DynamoDB.DocumentClient();

    docClient.query(params, function(err, data) {
       if (err) callback(err);
       else callback(null, data);
    });
}
