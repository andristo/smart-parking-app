var AWS = require('aws-sdk');

exports.handler = function(event,context,callback) {

    //console.log('Received event:', JSON.stringify(event, null, 2));
    
    var passedMeterId = event.meterId;
    console.log('-->passedMeterId: ' + passedMeterId);

    var params = {
      TableName: 'MeterStatus',
      KeyConditionExpression: 'MeterId = :meterId',
      ExpressionAttributeValues: {
        ':meterId': passedMeterId
      }
    };

    var docClient = new AWS.DynamoDB.DocumentClient();

    docClient.query(params, function(err, data) {
       if (err) callback(err);
       else callback(null, data);
    });
}
